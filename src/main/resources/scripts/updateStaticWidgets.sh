rm $1/$2/$2_de.png
rm $1/$2/$2_it.png
rm $1/$2/$2_en.png
rm $1/$2/$2_fr.png
rm $1/$2/$2_es.png
rm $1/$2/$2_ca.png
rm $1/$2/$2_oc.png

cp $1/$2/$3/$2_de.png $1/$2/
cp $1/$2/$3/$2_it.png $1/$2/
cp $1/$2/$3/$2_en.png $1/$2/
cp $1/$2/$3/$2_fr.png $1/$2/
cp $1/$2/$3/$2_es.png $1/$2/
cp $1/$2/$3/$2_ca.png $1/$2/
cp $1/$2/$3/$2_oc.png $1/$2/

chmod 755 $1/$2/$2_de.png
chmod 755 $1/$2/$2_it.png
chmod 755 $1/$2/$2_en.png
chmod 755 $1/$2/$2_fr.png
chmod 755 $1/$2/$2_es.png
chmod 755 $1/$2/$2_ca.png
chmod 755 $1/$2/$2_oc.png

